/**
 * Created by palepail on 2/1/2015.
 */
angular
    .module('com.palepail.website',
    [
        'ngResource',
        'ui.bootstrap',
        'ui.router',
        'com.palepail.website.navigation',
        'com.palepail.website.splash',
        'com.palepail.website.projects',
        'com.palepail.website.resume',
        'com.palepail.website.contact',
        'com.palepail.website.directives',
        'com.palepail.website.error',
        'com.palepail.website.sandbox',
        'com.palepail.website.palebot'
    ]);
angular
    .module('com.palepail.website')
    .constant('applicationName', 'palepail.com')
    .constant('versionNumber', 'v0.0.1')
    .constant('imagePath', 'resources/images/')
    .constant('palebotServer', 'http://localhost:8080/palebotV3')
    .constant('palebotWebSocket', 'ws://localhost:8080/palebotV3/websocket/id');
  //  .constant('palebotServer', 'http://palebot-palepail.rhcloud.com:8000/palebot')
 //   .constant('palebotWebSocket', 'ws://palebot-palepail.rhcloud.com:8000/palebot/channels');

(function () {
    function AppController($rootScope, $state, $scope, imagePath) {
        $rootScope.imagePath = imagePath;
        var keys;

        function initilaize() {
            $scope.cheatMode = false;
            keys = [];
        }

        angular.element(window).on('keydown', function (e) {
            keys.push(e.keyCode);
            if (keys.length > 10) {
                keys.shift();
            }

            if (keys.toString() == [38, 38, 40, 40, 37, 39, 37, 39, 66, 65].toString()) {

                $scope.cheatMode = true;
                $scope.$apply();
            }


        });


        $scope.getPageTitle = function () {
            if (!$state.current.data) {
                return null;
            }
            var pageSection = $state.current.data.pageSection ? ' - ' + $state.current.data.pageSection : '';
            return $state.current.data.pageTitle + pageSection;
        };
        initilaize()
    }

    angular.module('ng').filter('tel', function () {
        return function (tel) {
            if (!tel) {
                return '';
            }

            var value = tel.toString().trim().replace(/^\+/, '');

            if (value.match(/[^0-9]/)) {
                return tel;
            }

            var country, city, number;

            switch (value.length) {
                case 10: // +1PPP####### -> C (PPP) ###-####
                    country = 1;
                    city = value.slice(0, 3);
                    number = value.slice(3);
                    break;

                case 11: // +CPPP####### -> CCC (PP) ###-####
                    country = value[0];
                    city = value.slice(1, 4);
                    number = value.slice(4);
                    break;

                case 12: // +CCCPP####### -> CCC (PP) ###-####
                    country = value.slice(0, 3);
                    city = value.slice(3, 5);
                    number = value.slice(5);
                    break;

                default:
                    return tel;
            }

            if (country == 1) {
                country = "";
            }

            number = number.slice(0, 3) + '-' + number.slice(3);

            return (country + " (" + city + ") " + number).trim();
        };
    });

    angular
        .module('com.palepail.website')
        .controller('AppController', ['$rootScope', '$state', '$scope', 'imagePath', AppController]);
})();
