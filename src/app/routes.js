/**
 * Created by palepail on 2/1/2015.
 */
(function () {
    function Routes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                redirectTo: 'splash',
                data: {
                    pageTitle: 'Home'
                }
            })
            .state('splash', {
                url: '',
                templateUrl: 'modules/splash/SplashView.html',
                controller: 'SplashController',
                data: {
                    pageTitle: 'Home'
                }
            })
            .state('resume', {
                url: '/resume',
                templateUrl: 'modules/resume/ResumeView.html',
                controller: 'ResumeController',
                data: {
                    pageTitle: 'Resume'
                }
            })
            .state('projects', {
                url: '/projects',
                templateUrl: 'modules/project/ProjectGalleryView.html',
                controller: 'ProjectGalleryController',
                data: {
                    pageTitle: 'Projects'
                }
            })
            .state('projectDetails', {
                url: '/projects/:name',
                templateUrl: 'modules/project/ProjectDetailsView.html',
                controller: 'ProjectDetailsController',
                data: {
                    pageTitle: 'Projects'
                }
            })
            .state('contact', {
                url: '/contact',
                templateUrl: 'modules/contact/ContactView.html',
                data: {
                    pageTitle: 'Contact'
                }
            })
            .state('palebotLanding', {
                url: '/palebot',
                controller: 'PalebotLandingController',
                templateUrl: 'modules/palebot/palebotLandingView.html',
                data: {
                    pageTitle: 'Palebot'
                }
            })
            .state('palebotAdmin', {
                url: '/palebot/admin',
                controller: 'PalebotAdminController',
                templateUrl: 'modules/palebot/palebotAdminView.html',
                data: {
                    pageTitle: 'Palebot'
                }
            })
            .state('palebotUser', {
                url: '/palebot/:channelName',
                controller: 'PalebotUserController',
                templateUrl: 'modules/palebot/palebotUserView.html',
                data: {
                    pageTitle: 'Palebot'
                }
            })
            .state('palebotCommands', {
                url: '/palebot/commands/:channelName',
                controller: 'PalebotCommandsController',
                templateUrl: 'modules/palebot/palebotCommandsView.html',
                data: {
                    pageTitle: 'Palebot'
                }
            })
            .state('sandbox', {
                url: '/sandbox',
                templateUrl: 'modules/sandbox/SandboxHubView.html',
                controller: 'SandboxHubController',
                data: {
                    pageTitle: 'Sandbox'
                }
            })
            .state('sandboxItem', {
                url: '/sandbox/:name',
                templateUrl: function ($stateParams){
                    return 'modules/sandbox/' + $stateParams.name + '/index.html';
                },
                data: {
                    pageTitle: 'Sandbox'
                }
            })
            .state('error', {
                url: '/error',
                controller: 'ErrorController',
                templateUrl: 'modules/errors/ErrorView.html',
                data: {
                    pageTitle: 'Error'
                }
            });
        $urlRouterProvider.when('', 'splash');
        $urlRouterProvider.otherwise('/error');
    }

    angular
        .module('com.palepail.website')
        .config(['$stateProvider', '$urlRouterProvider', Routes]);
})();