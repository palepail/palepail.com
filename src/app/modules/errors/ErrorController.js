/**
 * Created by palepail on 7/10/2015.
 */
(function() {
    function ErrorController($scope) {
        initialize();

        function initialize() {

        }

    }
    angular
        .module("com.palepail.website.error")
        .controller("ErrorController", ['$scope', ErrorController]);
})();