/**
 * Created by palepail on 8/6/2015.
 */
(function() {
    function TwitchLoginController($scope,$modalInstance,$location, PalebotService) {


        function initialize() {

            initializeTwitch();

        }
        function initializeTwitch(){
            PalebotService.getTwitchClientId().then(function(result){

                Twitch.init({clientId: result,  redirect_uri: 'http://localhost:63342/palepail.com/src/app/index.html#/palebot'}, function(error, status) {
                    if (error) {
                        // error encountered while loading
                        console.info(error);
                    }
                    if (status.authenticated) {
                        console.info(status);
                    }
                });
            });
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.twitchLogin = function(){
            Twitch.login({
                scope: ['user_read', 'channel_read','chat_login'],
                redirect_uri: 'http://localhost:63342/palepail.com/src/app/index.html#/palebot'
            });
        }

        initialize();
    }
    angular
        .module("com.palepail.website.navigation")
        .controller("TwitchLoginController", ['$scope','$modalInstance','$location','PalebotService', TwitchLoginController]);
})();