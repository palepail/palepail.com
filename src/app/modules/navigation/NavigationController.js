(function() {
    function NavigationController($scope,$modal, NavigationService,PalebotService, applicationName) {
        $scope.applicationName = applicationName;
        $scope.goTo = NavigationService.goTo;
        $scope.isActive = NavigationService.isActive

        $scope.twitchLogin = function(){
            PalebotService.getTwitchClientId().then(function(result){
                Twitch.init({clientId: result,  redirect_uri: 'http://localhost:63342/palepail.com/src/app/index.html#/palebot'}, function(error, status) {
                    if (error) {
                        // error encountered while loading
                        console.info(error);
                    }
                    if (status.authenticated) {
                        NavigationService.goTo('palebotLanding');
                    }else{
                        var modalInstance = $modal.open({
                            templateUrl: 'TwitchLoginView.html',
                            controller: 'TwitchLoginController'
                        });
                        modalInstance.result.then(function (result) {

                        }, function () {

                        });
                    }
                });
            });

        }

    }
    angular
        .module('com.palepail.website.navigation')
        .controller('NavigationController', ['$scope','$modal', 'NavigationService','PalebotService','applicationName', NavigationController]);
})();