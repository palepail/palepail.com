(function() {
    function NavigationService($location,$state, $stateParams) {
        this.goTo = function(route, params) {
            $state.go(route, params)
        };

        this.isActive = function(route) {
            return $location.path() === route || $location.path().indexOf(route) === 0;
        };

        this.getParameter = function(parameterName) {
            return $stateParams[parameterName];
        };

        this.setParameter = function(parameterName, value) {
            $stateParams[parameterName] = value;
        };
    }
    angular
        .module('com.palepail.website.navigation')
        .service('NavigationService', ['$location','$state', '$stateParams', NavigationService]);
})();