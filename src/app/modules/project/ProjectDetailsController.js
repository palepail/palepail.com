(function() {
    function ProjectDetailsController($scope, $state, NavigationService, ProjectService) {
        initialize();
        function initialize() {
            var projectName = NavigationService.getParameter('name');
            $state.current.data.pageSection = projectName;
            ProjectService.getProjectWithName(projectName).then(function(project) {
                $scope.project = project;
            });
        }

        $scope.isString = function(object) {
            return angular.isString(object);
        }
    }
    angular
        .module("com.palepail.website.projects")
        .controller("ProjectDetailsController", ['$scope', '$state', 'NavigationService', 'ProjectService', ProjectDetailsController]);
})();