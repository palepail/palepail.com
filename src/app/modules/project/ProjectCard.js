(function() {
    function ProjectThumbnail() {
        function ProjectThumbnailController($scope, NavigationService, imagePath) {
            $scope.imagePath = imagePath;
            $scope.showProject = function(project) {
                NavigationService.goTo('/projects/' + project.name);
            }
        }

        return {
            restrict: "A",
            scope: {
                project: "="
            },
            templateUrl: "ProjectCard.html",
            controller: ['$scope', 'NavigationService', 'imagePath', ProjectThumbnailController]
        };
    }
    angular
        .module("com.palepail.website.projects")
        .directive("projectCard", ProjectThumbnail);
})();