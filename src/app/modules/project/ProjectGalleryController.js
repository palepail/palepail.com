(function () {
    function ProjectGalleryController ($scope, ProjectService) {
        $scope.projects = [];
        initialize();
        function initialize() {
            ProjectService.getProjects().then(function(projects) {
                $scope.projects = projects;
            });
        }

    }

    angular
        .module('com.palepail.website.projects')
        .controller('ProjectGalleryController', ['$scope', 'ProjectService', ProjectGalleryController]);
})();
