(function () {
    function SplashService($http, $q) {

        var tabs = [];

        this.getTabs = function() {
            var deferred = $q.defer();
            if (tabs.length === 0) {
                $http.get('data/splashtabs.json')
                    .then(function(response) {
                        tabs = response.data;
                        deferred.resolve(tabs);
                    });
            }
            else {
                deferred.resolve(tabs);
            }
            return deferred.promise;
        };

    }

    angular
        .module('com.palepail.website.splash')
        .service('SplashService', ['$http', '$q', SplashService]);
})();
