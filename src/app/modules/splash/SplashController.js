(function () {
    function SplashController($scope, SplashService,ProjectService, NavigationService) {

        initialize()

        function initialize(){
            SplashService.getTabs().then(function (tabs) {
                $scope.tabs = tabs;
            }).then(function(){
                ProjectService.getProjects().then(function(projects){
                    $scope.projects = projects;
                })
            });

        }



        $scope.showProject = function (slide) {
            NavigationService.goTo('/projects/' + slide.project);
        };

        $scope.showPost = function (post) {
            NavigationService.goTo('/blog/posts/' + post.url);
        };
    }

    angular
        .module('com.palepail.website.splash', [])
        .controller('SplashController', ['$scope', 'SplashService','ProjectService','NavigationService', SplashController]);
})();
