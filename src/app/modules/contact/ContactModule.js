/**
 * Created by derek.kam on 6/23/2015.
 */
(function () {
    angular
        .module('com.palepail.website.contact', ['com.palepail.website.navigation']);
})();
