/**
 * Created by derek.kam on 6/23/2015.
 */
(function() {
    function ContactController($scope, ResumeService) {
        initialize();
        $scope.resume = {};
        function initialize() {
            ResumeService.getResume().then(function(resume) {
                $scope.resume = resume.sections;
                console.info($scope.resume);
            });
        }

    }
    angular
        .module("com.palepail.website.contact")
        .controller("ContactController", ['$scope', 'ResumeService', ContactController]);
})();