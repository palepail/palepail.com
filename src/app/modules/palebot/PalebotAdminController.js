/**
 * Created by palepail on 7/26/2015.
 */
(function () {
    function PalebotAdminController($scope, $sce, $state, $modal, $interval, $timeout, NavigationService, PalebotService) {


        $scope.currentChat;
        $scope.KEYS = {CONNECTED: "CONNECTED", DEFAULT_CHAT: "palebot"};
        $scope.allListeners = [];

        function initialize() {
            var projectName = NavigationService.getParameter('name');

            $state.current.data.pageSection = projectName;
            $scope.initializeChannels();

            PalebotService.getListenerInfo().then(function (listeners) {
                $scope.allListeners = listeners;
            })
            checkTwitchLogin();
            $interval(function () {
                $scope.getChannels();
            }, 10000);


        }

        $scope.goToPalebotUser = function(){
            NavigationService.goTo("palebotUser", $scope.twitchUser);
        }



        function checkTwitchLogin(){
            PalebotService.getTwitchClientId().then(function(result){
                Twitch.init({clientId: result,  redirect_uri: 'http://localhost:63342/palepail.com/src/app/index.html#/palebot'}, function(error, status) {
                    if (error) {
                        // error encountered while loading
                        console.info(error);
                    }
                    if (!status.authenticated) {
                        NavigationService.goTo('splash');
                        $scope.stopCheckChannel();
                    }else{

                        Twitch.api({method: 'user'}, function(error, user) {
                            $scope.twitchUser = user.name;

                                PalebotService.isPalebotAdmin($scope.twitchUser).then(function(result){
                                    console.info(result);
                                    if(result!="true")
                                    {
                                        NavigationService.goTo("splash");
                                    }else{

                                    }
                                })


                        });

                        Twitch.events.addListener('auth.logout', function() {
                            NavigationService.goTo('splash');
                            $scope.stopCheckChannel();
                        });
                    }
                });
            });
        }


        $scope.palebotToggle = function (channel) {
            loadingTimeOut();
            if (channel.status != $scope.KEYS.CONNECTED) {
                $scope.toggleOn(channel);
            } else {
                $scope.toggleOff(channel);
            }

        }

        $scope.toggleOn = function (channel) {

            PalebotService.turnOn(channel.name).then(function (result) {
                channel = result;
                if ($scope.loading == true) {
                    $scope.loading = false;
                }
            });
        }
        $scope.toggleOff = function (channel) {
            PalebotService.turnOff(channel.name).then(function (result) {
                channel = result;
                if ($scope.loading == true) {
                    $scope.loading = false;
                }
            });
        }
        $scope.toggleAllOff = function () {
            loadingTimeOut();
            angular.forEach($scope.channels, function (channel) {
                $scope.toggleOff(channel);
            })

        }

        $scope.toggleAllOn = function () {
            loadingTimeOut();

            angular.forEach($scope.channels, function (channel) {
                $scope.toggleOn(channel);

            })
        }

        $scope.initializeChannels = function () {
            console.info("getting channels");
            PalebotService.getChannels().then(function (channels) {
            console.info(channels);
                if (channels == "ERROR") {
                    $scope.setChat($scope.KEYS.DEFAULT_CHAT);
                }else{
                    $scope.channels = channels;
                    $scope.currentChannel = $scope.channels[0];
                    $scope.setChat($scope.currentChannel.name);

                }

            });

        }

        $scope.getChannels = function () {

            PalebotService.getChannels().then(function (channels) {
                if (channels == "ERROR") {
                    angular.forEach($scope.channels, function (channel) {
                        channel.status = 'UNKOWN';
                    })
                } else {
                    $scope.channels = channels;
                    if (!$scope.currentChannel) {
                        $scope.changeCurrentChannel($scope.channels[0]);
                    }

                    if ($scope.loading == true) {
                        $scope.loading = false;
                    }
                }
            });
        }


        $scope.addChannel = function () {
            var modalInstance = $modal.open({
                templateUrl: 'AddChannel.html',
                controller: 'AddChannelController',
                resolve: {
                    channels: function () {
                        return $scope.channels;
                    }
                }
            });
            modalInstance.result.then(function (channels) {
                $scope.channels = channels;
            }, function () {

            });
        }

        $scope.changeCurrentChannel = function (channel) {
            $scope.currentChannel = channel;
            $scope.setChat($scope.currentChannel.name)

        }


        $scope.setChat = function (channelName) {

            $scope.currentChat = $sce.trustAsResourceUrl("http://www.twitch.tv/" + channelName + "/chat");
            $scope.chatChanged = true;

        }

        $scope.removeChannel = function (channel) {
            loadingTimeOut();
            console.info("removing channel id:" + channel.id);
            PalebotService.removeChannel(channel.id).then(function (channels) {
                $scope.channels = channels;
                $scope.currentChannel = $scope.channels[0];
                $scope.loading = false;
            });

        }

        $scope.toggleListenerOff =function(channel, listener)
        {
            loadingTimeOut();

            PalebotService.toggleListenerOff(channel, listener).then(function (result) {
                channel = result;
                $scope.loading = false;
            })
        }

        $scope.toggleListenerOn =function(channel, listener)
        {
            loadingTimeOut();
            PalebotService.toggleListenerOn(channel, listener).then(function (result) {
                channel = result;
                $scope.loading = false;
            })

        }


        $scope.ifListenersContains = function (listeners, listenerToFind) {
            var contains = false;


            angular.forEach(listeners, function(listenerInArray){

                if(listenerInArray.name.toUpperCase() == listenerToFind.name.toUpperCase())
                {
                    contains = true;
                }
            })
            return contains;

        }

        $scope.isString = function (object) {
            return angular.isString(object);
        }



        function loadingTimeOut() {
            $scope.loading = true;
            $timeout(function () {
                if ($scope.loading == true) {
                    $scope.loading = false;
                }
            }, 10000);

        }


        initialize();
    }

    angular
        .module("com.palepail.website.palebot")
        .controller("PalebotAdminController", ['$scope', '$sce', '$state', '$modal', '$interval', '$timeout', 'NavigationService', 'PalebotService', PalebotAdminController]);
})();