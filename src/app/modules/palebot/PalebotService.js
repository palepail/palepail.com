/**
 * Created by palepail on 7/26/2015.
 */
(function () {
    function PalebotService($http, $q, palebotServer) {

        var listeners =[];
        var channels =[];
        var links =
        {
            turnOn: function (channelName) {
                return palebotServer + "/palebot/on?channel=" + channelName;
            },
            turnOff: function (channelName) {
                return palebotServer + "/palebot/off?channel=" + channelName;
            },
            getWaifuByChannel: function(channelId){
                return palebotServer + "/palebot/anime/waifu?channelId=" + channelId;
            },
            saveWaifu: function(channelId){
                return palebotServer + "/palebot/anime/waifu?channelId=" + channelId;
            },
            isOn: function (channelName) {
                return palebotServer + "/palebot/isOn?channel=" + channelName;
            }
            ,isPalebotAdmin: function(userName){
                return palebotServer + "/palebot/isPalebotAdmin?userName="+userName;
            },
            getAllChannels: function () {
                return palebotServer + "/palebot/channels";
            },
            getAllListeners: function () {
                return palebotServer + "/palebot/listeners";
            },
            getChannelByName: function (channelName) {
                return palebotServer + "/palebot/channels/name/" + channelName;
            },
            addChannel: function () {
                return palebotServer + "/palebot/channels";
            },
            removeChannel: function (id) {
                return palebotServer + "/palebot/channels/" + id;
            },
            toggleListenerOn: function (channelId, listenerName) {
                return palebotServer + "/palebot/" + channelId + "/toggleListenerOn?listenerName=" + listenerName;
            },
            toggleListenerOff: function (channelId, listenerName) {
                return palebotServer + "/palebot/" + channelId + "/toggleListenerOff?listenerName=" + listenerName;
            },
            toggleAllOff: function () {
                return palebotServer + "/palebot/allOff/";
            },
            toggleAllOn: function () {
                return palebotServer + "/palebot/allOn/";
            },
            checkTwitchChannel: function (channelName) {
                return " https://api.twitch.tv/kraken/channels/" + channelName + "?callback=JSON_CALLBACK";
            },
            getTwitchClientId: function (){
                return palebotServer + "/palebot/twitch/clientId";
            }

        }

        this.saveWaifu = function(channelId, waifu){
            var deferred = $q.defer();
            $http.post(links.saveWaifu(channelId),waifu)
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }

        this.getTwitchClientId = function(){
            var deferred = $q.defer();
            $http.get(links.getTwitchClientId())
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }

        this.isPalebotAdmin = function(userName){
            var deferred = $q.defer();
            $http.get(links.isPalebotAdmin(userName))
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }

        this.toggleListenerOn = function (channel, listener) {
            var deferred = $q.defer();
            $http.get(links.toggleListenerOn(channel.id, listener.name))
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }
        this.toggleListenerOff = function (channel, listener) {
            var deferred = $q.defer();
            $http.get(links.toggleListenerOff(channel.id, listener.name))
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }

        this.getAllListeners = function () {
            var deferred = $q.defer();
            $http.get(links.getAllListeners())
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;

        }

        this.getWaifuByChannel = function(channelId){
            var deferred = $q.defer();
            $http.get(links.getWaifuByChannel(channelId))
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function (response) {
                    deferred.resolve("ERROR");
                });

            return deferred.promise;
        }

        this.getChannelByName = function (channelName) {
            var deferred = $q.defer();
            $http.get(links.getChannelByName(channelName))
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function (response) {
                    deferred.resolve("ERROR");
                });

            return deferred.promise;

        }



        this.getChannels = function () {

            var deferred = $q.defer();
            $http.get(links.getAllChannels())
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function (response) {
                    deferred.resolve("ERROR");
                });

            return deferred.promise;
        };

        this.addChannel = function (channel) {
            var deferred = $q.defer();

            $http.post(links.addChannel(), channel)
                .then(function (response) {

                    deferred.resolve(response.data);
                });

            return deferred.promise;
        };

        this.checkTwitchChannel = function (channelName) {
            var deferred = $q.defer();
            $http.jsonp(links.checkTwitchChannel(channelName))
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;

        }

        this.toggleAllOn = function () {
            var deferred = $q.defer();
            $http.post(links.toggleAllOn())
                .then(function (response) {
                    deferred.resolve(response.data);
                });
            return deferred.promise;
        }
        this.toggleAllOff = function () {
            var deferred = $q.defer();
            $http.post(links.toggleAllOff())
                .then(function (response) {
                    deferred.resolve(response.data);
                });
            return deferred.promise;
        }


        this.turnOn = function (channelName) {
            var deferred = $q.defer();
            $http.post(links.turnOn(channelName))
                .then(function (response) {
                    deferred.resolve(response.data);
                });
            return deferred.promise;
        };

        this.turnOff = function (channelName) {
            var deferred = $q.defer();
            $http.post(links.turnOff(channelName))
                .then(function (response) {
                    deferred.resolve(response.data);
                });
            return deferred.promise;
        }

        this.removeChannel = function (id) {
            var deferred = $q.defer();
            $http.delete(links.removeChannel(id))
                .then(function (response) {
                    deferred.resolve(response.data);
                });
            return deferred.promise;

        }

        this.getListenerInfo = function() {
            var deferred = $q.defer();
            if (listeners.length === 0) {
                $http.get('data/palebot/listeners.json')
                    .then(function(response) {
                        listeners = response.data;
                        deferred.resolve(listeners);
                    });
            }
            else {
                deferred.resolve(listeners);
            }
            return deferred.promise;
        };

        this.getChannelInfo = function(){

            var deferred = $q.defer();
            if (channels.length === 0) {
                $http.get('data/palebot/channels.json')
                    .then(function(response) {
                        channels = response.data;
                        deferred.resolve(channels);
                    });
            }
            else {
                deferred.resolve(channels);
            }
            return deferred.promise;

        };



    }

    angular
        .module('com.palepail.website.palebot')
        .service('PalebotService', ['$http', '$q', 'palebotServer', PalebotService]);
})();
