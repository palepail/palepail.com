/**
 * Created by palepail on 7/27/2015.
 */
(function() {
    function AddChannelController($scope,$modalInstance,$q,$filter,channels, PalebotService) {

        $scope.loading = false;
        $scope.channel ='';

        function initialize() {

        }

        $scope.channels = channels;


        $scope.add = function () {
            $scope.loading=true;
            $scope.errorMessage = '';
            PalebotService.checkTwitchChannel($scope.channel).then(function(fullChannel) {
                console.info(fullChannel)
                if(fullChannel._id){
                    return fullChannel;
                }
                else{
                    return $q.reject('Channel Not Found');
                }
            }).then(function(fullChannel){

                if($filter('filter')($scope.channels, {name: fullChannel.name}).length >0)
                {
                    return $q.reject('Already Joined');
                }
                console.info("adding channel")
                var channel = {name:fullChannel.name}
                PalebotService.addChannel(channel).then(function(result){
                    console.info(result);
                    $scope.channels.push(result);
                    $scope.loading=false;
                    $modalInstance.close($scope.channels);
                })


            }).catch(function(error){
                $scope.loading=false;
                $scope.errorMessage = error;
            });

        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };


        function getByValue(arr, value) {

            var result  = arr.filter(function(o){return o.b == value;} );

            return result? result[0] : null;

        }


        initialize();
    }
    angular
        .module("com.palepail.website.palebot")
        .controller("AddChannelController", ['$scope','$modalInstance','$q','$filter','channels', 'PalebotService', AddChannelController]);
})();