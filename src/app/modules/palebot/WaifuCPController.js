/**
 * Created by palepail on 7/27/2015.
 */
(function () {
    function WaifuCPController($scope, $modalInstance, channelId, PalebotService) {

        $scope.loading = false;
        $scope.channel = '';
        $scope.waifuList = [];
        $scope.maxDisplayedPages = 10;
        $scope.maxPerPage = 5;
        $scope.currentPage = 1;
        $scope.seachCriteria = '';
        $scope.edit = {id: '', deleting: []};

        function initialize() {
            $scope.loading = true;
            PalebotService.getWaifuByChannel(channelId).then(function (result) {
                $scope.channelWaifu = result;

                $scope.loading = false;
            });
        }

        $scope.saveWaifu = function () {
            $scope.edit.id = '';
            $scope.loading = true;
            console.info($scope.channelWaifu)
            $scope.removeWaifu();
            console.info($scope.channelWaifu)
            PalebotService.saveWaifu(channelId, $scope.channelWaifu).then(function (result) {
                $scope.loading = false;
                $scope.channelWaifu = result;
            })
        }
        $scope.toggleDelete = function (waifu) {

            if (waifu.DELETE == true) {
                $scope.edit.deleting = $scope.edit.deleting.filter(function (returnableObjects) {
                    return returnableObjects !== waifu.id;
                });

                waifu.DELETE = false;
            } else {
                $scope.edit.deleting.push(waifu.id);
                waifu.DELETE = true;
            }

        }

        $scope.removeWaifu = function () {

            angular.forEach($scope.edit.deleting, function (waifuToDelete) {
                $scope.channelWaifu = $scope.channelWaifu.filter(function (returnableObjects) {
                    return returnableObjects.id !== waifuToDelete;
                });
            })
        }

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.paginate = function (list) {
            var begin = (($scope.currentPage - 1) * $scope.maxPerPage);
            var end = begin + $scope.maxPerPage;
            return list.slice(begin, end);
        }

        initialize();
    }

    angular
        .module("com.palepail.website.palebot")
        .controller("WaifuCPController", ['$scope', '$modalInstance', 'channelId', 'PalebotService', WaifuCPController]);
})();