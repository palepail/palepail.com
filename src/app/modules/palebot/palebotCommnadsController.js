/**
 * Created by palepail on 8/31/2015.
 */

(function () {
    function PalebotCommandsController($scope, $sce, $modal, NavigationService, PalebotService) {


        $scope.allListeners = [];
        $scope.listeners = [];
        $scope.channels = [];

        $scope.checkChannel;
        function initialize() {
            $scope.websocketOpen=false;
            $scope.playListLimit = 30;
            $scope.admin = false;
            $scope.channelName = NavigationService.getParameter('channelName');
            $scope.error = false;

            PalebotService.getListenerInfo().then(function(result){
                $scope.listeners = result;
                console.info("listeners");
                console.info($scope.listeners);

            });

            $scope.initializeChannel($scope.channelName)


        }

        function getByName (array, nameToFind) {
        var item={};
            angular.forEach(array, function (itemInArray) {
                if (itemInArray.name.toUpperCase() == nameToFind.toUpperCase()) {
                    item = itemInArray;
                }
            })
            return item;
        }

        $scope.getChannelListeners = function(){
            var channelListeners = [];
            if($scope.channel) {

                angular.forEach($scope.channel.listeners, function (serverListener) {
                    angular.forEach($scope.listeners, function (listenerData) {
                        if (listenerData.name.toUpperCase() == serverListener.name.toUpperCase()) {
                            channelListeners.push(listenerData);
                        }
                    })
                })
            }
            return channelListeners;
        };


        $scope.initializeChannel = function (channelName) {

            PalebotService.getChannelInfo().then(function (channels) {

                if (!channels || channels == "ERROR") {
                    $scope.error=true;
                } else if(channels) {
                    $scope.channel = getByName(channels, channelName);
                    console.info($scope.channel);
                    if (! $scope.channel) {
                    $scope.error = true;
                     }

                }else(

                    $scope.addChannel(channelName)
                )

            });



        };



        $scope.ifListenersContains = function (listeners, listenerToFind) {
            var contains = false;
            angular.forEach(listeners, function (listenerInArray) {
                if (listenerInArray.name == listenerToFind) {
                    contains = true;
                }
            })
            return contains;
        };

        $scope.isString = function (object) {
            return angular.isString(object);
        }



        initialize();
    }

    angular
        .module("com.palepail.website.palebot")
        .controller("PalebotCommandsController", ['$scope', '$sce', '$modal', 'NavigationService', 'PalebotService', PalebotCommandsController]);
})();