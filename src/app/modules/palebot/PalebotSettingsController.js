/**
 * Created by palepail on 8/19/2015.
 */
(function() {
    function PalebotSettingsController($scope,$modalInstance,$q,$filter,channel, allListeners, PalebotService, NavigationService) {

        $scope.loading = false;


        function initialize() {
            $scope.channel = channel;
            $scope.allListeners = allListeners;
        }

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
$scope.setInfo = function(info)
{
    console.info("called")
    $scope.listenerInfo = info;
}

        $scope.ifListenersContains = function (listeners, listenerToFind) {
            var contains = false;
            angular.forEach(listeners, function (listenerInArray) {
                if (listenerInArray.name == listenerToFind) {
                    contains = true;
                }
            })
            return contains;
        }

        $scope.removeChannel = function (channel) {

            PalebotService.removeChannel(channel.id).then(function (channel) {
                $scope.loading = false;
                NavigationService.goTo('splash');
            });

        }

        initialize();
    }
    angular
        .module("com.palepail.website.palebot")
        .controller("PalebotSettingsController", ['$scope','$modalInstance','$q','$filter','channel','allListeners', 'PalebotService','NavigationService', PalebotSettingsController]);
})();