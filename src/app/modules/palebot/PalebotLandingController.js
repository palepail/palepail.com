/**
 * Created by palepail on 8/6/2015.
 */

(function () {
    function PalebotLandingController($scope,$state, NavigationService, PalebotService) {






        function initialize() {
            PalebotService.getTwitchClientId().then(function(result) {

                Twitch.init({
                    clientId: result,
                    redirect_uri: 'http://localhost:63342/palepail.com/src/app/index.html#/palebot'
                }, function (error, status) {
                    if (error) {
                        // error encountered while loading
                        console.info(error);
                    }
                    if (status.authenticated) {
                    }
                });

                Twitch.api({method: 'user'}, function(error, user) {
                  if(user){
                      console.info(user);
                      $state.go('palebotUser',{channelName: user.name})
                  }
                });
            });



            PalebotService.getAllListeners().then(function (listeners) {
                $scope.allListeners = listeners;
            })


        }



        initialize();
    }

    angular
        .module("com.palepail.website.palebot")
        .controller("PalebotLandingController", ['$scope','$state','NavigationService', 'PalebotService', PalebotLandingController]);
})();