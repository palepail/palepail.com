/**
 * Created by palepail on 7/26/2015.
 */
(function () {
    function PalebotUserController($scope, $sce, $modal, $interval, $timeout,palebotWebSocket, NavigationService, PalebotService, YoutubeService) {

        var ws;

        $scope.currentChat;
        $scope.twitchUser;
        $scope.channelName
        $scope.admin;
        $scope.KEYS = {CONNECTED: "CONNECTED", DEFAULT_CHAT: "palebot"};
        $scope.allListeners = [];
        $scope.videos=[];

        $scope.checkChannel;
        function initialize() {
            $scope.websocketOpen=false;
            $scope.playListLimit = 30;
            $scope.admin = false;
            $scope.channelName = NavigationService.getParameter('channelName');


            checkTwitchLogin();

            PalebotService.getAllListeners().then(function (listeners) {
                $scope.allListeners = listeners;
            })

            $scope.checkChannel = $interval(function () {
                $scope.getChannelByName($scope.channelName);
                if(YoutubeService.getWebSocket().readyState==WebSocket.CLOSED){
                    initializeWebSocket();
                }
            }, 10000);
        }


        $scope.$on('$stateChangeStart', function( event ) {
            if(ws)
            {
                ws.close();
            }
            $scope.stopCheckChannel();
        });


        $scope.openSettings = function (channel) {
            var modalInstance = $modal.open({
                templateUrl: 'PalebotSettingsView.html',
                controller: 'PalebotSettingsController',
                size: 'lg',
                resolve: {
                    channel: function () {
                        return channel;
                    },
                    allListeners: function () {
                        return $scope.allListeners;
                    }
                }
            });
            modalInstance.result.then(function (result) {

            }, function () {

            });
        }

        $scope.stopCheckChannel = function () {
            if (angular.isDefined($scope.checkChannel)) {
                $interval.cancel($scope.checkChannel);
                $scope.checkChannel = undefined;
            }
        };

        function checkTwitchLogin() {
            PalebotService.getTwitchClientId().then(function (result) {
                Twitch.init({
                    clientId: result,
                    redirect_uri: 'http://localhost:63342/palepail.com/src/app/index.html#/palebot'
                }, function (error, status) {
                    if (error) {
                        // error encountered while loading
                        console.info(error);
                    }
                    if (!status.authenticated) {
                        NavigationService.goTo('splash');
                    } else {
                        Twitch.api({method: 'user'}, function (error, user) {
                            $scope.twitchUser = user.name;
                            if ($scope.twitchUser != $scope.channelName) {
                                NavigationService.goTo('palebotLanding');
                            } else {

                                $scope.initializeChannel($scope.twitchUser);
                                PalebotService.isPalebotAdmin($scope.twitchUser).then(function (result) {
                                    if (result == "true") {
                                        $scope.admin = true;
                                    }
                                })
                            }
                        });

                        Twitch.events.addListener('auth.logout', function () {
                            NavigationService.goTo('splash');
                        });
                    }
                });
            });
        }

        $scope.twitchLogout = function () {
            Twitch.logout();
            NavigationService.goTo('splash');
        }

        $scope.palebotToggle = function (channel) {
            loadingTimeOut();
            if (channel.status != $scope.KEYS.CONNECTED) {
                $scope.toggleOn(channel);
            } else {
                $scope.toggleOff(channel);
            }

        }

        $scope.toggleOn = function (channel) {
            console.info("toggle on")
            PalebotService.turnOn(channel.name).then(function (result) {
                $scope.channel = result;
                $scope.currentChannel = $scope.channel;
                if ($scope.loading == true) {
                    $scope.loading = false;
                }
            });

        }
        $scope.toggleOff = function (channel) {
            console.info("toggle off")
            PalebotService.turnOff(channel.name).then(function (result) {

                $scope.channel = result;
                $scope.currentChannel = $scope.channel;
                if ($scope.loading == true) {
                    $scope.loading = false;
                }
            });
        }

        $scope.goToPalebotAdmin = function () {
            PalebotService.isPalebotAdmin($scope.twitchUser).then(function (result) {
                if (result == "true") {
                    NavigationService.goTo("palebotAdmin");
                } else {
                }
            })
        }

        $scope.initializeChannel = function (channelName) {

            PalebotService.getChannelByName(channelName).then(function (channel) {

                console.info(channel)
                if (channel == "ERROR") {
                    $scope.setChat($scope.KEYS.DEFAULT_CHAT);
                } else if(channel){

                    $scope.channel = channel;
                    $scope.currentChannel = $scope.channel;
                    $scope.setChat($scope.currentChannel.name);
                    initializeWebSocket();

                }else(

                    $scope.addChannel(channelName)
                )

            });

        }

        function initializeWebSocket(){
            YoutubeService.setWebsocket(new WebSocket(palebotWebSocket+"/"+$scope.channel.id));
        }



        $scope.addChannel = function (channelName) {
            loadingTimeOut();
            console.info("adding channel")
            console.info(channelName)
            var channel = {name:$scope.twitchUser};
            PalebotService.addChannel(channel).then(function(channel){
                $scope.channel = channel;
                $scope.currentChannel = $scope.channel;
                $scope.setChat($scope.currentChannel.name);

            })
        }

        $scope.openWaifuCP = function () {
            var modalInstance = $modal.open({
                templateUrl: 'WaifuCPView.html',
                controller: 'WaifuCPController',
                size:'lg',
                resolve: {
                    channelId: function () {
                        return $scope.channel.id;
                    }
                }
            });
            modalInstance.result.then(function (channels) {
                $scope.channels = channels;
            }, function () {

            });
        }

        $scope.getChannelByName = function (channelName) {

            PalebotService.getChannelByName(channelName).then(function (channel) {
                if (channel == "ERROR") {
                    $scope.channel.status = 'UNKOWN';

                } else {
                    $scope.channel = channel;

                    $scope.changeCurrentChannel($scope.channel);

                    if(!$scope.currentChat)
                    {
                        $scope.setChat(channel.name)
                    }

                    if ($scope.loading == true) {
                        $scope.loading = false;
                    }
                }
            });
        }


        $scope.setChat = function (channelName) {

            $scope.currentChat = $sce.trustAsResourceUrl("http://www.twitch.tv/" + channelName + "/chat");

            $scope.chatChanged = true; //used to refresh iframe

        }



        $scope.toggleListenerOff = function (channel, listener) {
            loadingTimeOut();
            PalebotService.toggleListenerOff(channel, listener).then(function (result) {

                console.info(result);
                channel = result;
                $scope.loading = false;
            })
        }

        $scope.toggleListenerOn = function (channel, listener) {
            loadingTimeOut();
            PalebotService.toggleListenerOn(channel, listener).then(function (result) {
                channel = result;
                $scope.loading = false;
            })

        }

        $scope.changeCurrentChannel = function (channel) {
            $scope.currentChannel = channel;
            $scope.setChat($scope.currentChannel.name)

        }

        $scope.ifListenersContains = function (listeners, listenerToFind) {
            var contains = false;
            angular.forEach(listeners, function (listenerInArray) {
                if (listenerInArray.name == listenerToFind) {
                    contains = true;
                }
            })
            return contains;
        }

        $scope.isString = function (object) {
            return angular.isString(object);
        }

        function loadingTimeOut() {
            $scope.loading = true;
            $timeout(function () {
                if ($scope.loading == true) {
                    $scope.loading = false;
                }
            }, 10000);

        }

        initialize();
    }

    angular
        .module("com.palepail.website.palebot")
        .controller("PalebotUserController", ['$scope', '$sce', '$modal', '$interval', '$timeout','palebotWebSocket', 'NavigationService', 'PalebotService','YoutubeService', PalebotUserController]);
})();