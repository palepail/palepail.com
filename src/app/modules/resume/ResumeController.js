/**
 * Created by derek.kam on 6/19/2015.
 */
(function() {
    function ResumeController($scope, ResumeService) {
        initialize();
        $scope.resume = {};
        function initialize() {
            ResumeService.getResume().then(function(resume) {
                $scope.resume = resume.sections;
                console.info($scope.resume);
            });
        }

    }
    angular
        .module("com.palepail.website.resume")
        .controller("ResumeController", ['$scope', 'ResumeService', ResumeController]);
})();