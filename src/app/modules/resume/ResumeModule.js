/**
 * Created by derek.kam on 6/19/2015.
 */
(function () {
    angular
        .module('com.palepail.website.resume', ['com.palepail.website.navigation']);
})();
