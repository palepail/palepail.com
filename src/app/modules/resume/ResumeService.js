/**
 * Created by derek.kam on 6/19/2015.
 */
(function () {
    function ResumeService($http, $q) {
        var resume = {};


        this.getResume = function() {
            var deferred = $q.defer();
            if (Object.getOwnPropertyNames(resume).length <= 0) {
                $http.get('data/resume.json')
                    .then(function(response) {
                        console.info(response);
                        resume = response.data;
                        deferred.resolve(resume);
                    });
            }
            else {
                deferred.resolve(resume);
            }
            return deferred.promise;
        };


    }

    angular
        .module('com.palepail.website.resume')
        .service('ResumeService', ['$http', '$q', ResumeService]);
})();
