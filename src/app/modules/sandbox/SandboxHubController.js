/**
 * Created by palepail on 7/14/2015.
 */
(function () {
    function SandboxHubController ($scope, SandboxHubService) {
        $scope.sandbox = [];
        initialize();
        function initialize() {
            SandboxHubService.getItems().then(function(sandbox) {
                $scope.sandbox = sandbox;
            });
        }

    }

    angular
        .module('com.palepail.website.sandbox')
        .controller('SandboxHubController', ['$scope', 'SandboxHubService', SandboxHubController]);
})();
