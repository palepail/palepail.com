/**
 * Created by palepail on 7/14/2015.
 */
(function () {
    function SandboxHubService($http, $q) {
        var sandbox = [];


        this.getItems = function() {
            var deferred = $q.defer();
            if (sandbox.length === 0) {
                $http.get('data/sandbox.json')
                    .then(function(response) {
                        sandbox = response.data;
                        deferred.resolve(sandbox);
                    });
            }
            else {
                deferred.resolve(sandbox);
            }
            return deferred.promise;
        };

        this.getItemWithName = function(name) {
            var deferred = $q.defer();
            this.getItems().then(function(sandbox) {
                var match = null;
                angular.forEach(sandbox, function(item, index) {
                    if (!match && item.name === name) {
                        match = item;
                    }
                });
                deferred.resolve(match);
            });
            return deferred.promise;
        };


    }

    angular
        .module('com.palepail.website.sandbox')
        .service('SandboxHubService', ['$http', '$q', SandboxHubService]);
})();
