/**
 * Created by palepail on 7/14/2015.
 */
(function() {
    function ItemCard() {
        function ItemCardController($scope, NavigationService, imagePath) {
            $scope.imagePath = imagePath;
            $scope.showItem = function(item) {
                NavigationService.goTo('/sandbox/' + item.pathName);
            }
        }

        return {
            restrict: "A",
            scope: {
                item: "="
            },
            templateUrl: "ItemCard.html",
            controller: ['$scope', 'NavigationService', 'imagePath', ItemCardController]
        };
    }
    angular
        .module("com.palepail.website.sandbox")
        .directive("itemCard", ItemCard);
})();