/**
 * Created by palepail on 8/16/2015.
 */

(function() {
    function YoutubeService($window,$rootScope,$log,$http, $q) {

        var service = this;
        var ApiKey = 'AIzaSyCKaaNtf0EDIlbKePeJTzAhfzFyNkQJ0-M';
        var websocket = {};

        var links = {
            getVideoData: function (videoId) {
            return  "https://www.googleapis.com/youtube/v3/videos?id=" + videoId + "&key=" + ApiKey+"&part=snippet,contentDetails";
            }
        }

        var youtube = {
            ready: false,
            player: null,
            playerId: null,
            videoId: null,
            videoTitle: null,
            playerHeight: '360',
            playerWidth: '640',
            state: 'stopped'
        };
        var upcoming = [];
        var history = [];

        $window.onYouTubeIframeAPIReady = function () {
            console.info('Youtube API is ready');
            youtube.ready = true;
            service.bindPlayer('placeholder');
            service.loadPlayer();
            $rootScope.$apply();
        };

        function onYoutubeReady (event) {
            console.info('YouTube Player is ready');
            youtube.player.cueVideoById(history[0].id);
            youtube.videoId = history[0].id;
            youtube.videoTitle = history[0].title;
        }

        function onYoutubeStateChange (event) {
            if (event.data == YT.PlayerState.PLAYING) {
                youtube.state = 'playing';
            } else if (event.data == YT.PlayerState.PAUSED) {
                youtube.state = 'paused';
            } else if (event.data == YT.PlayerState.ENDED) {
                youtube.state = 'ended';
                service.launchPlayer(upcoming[0]);
                service.archiveVideo(upcoming[0]);
                service.deleteVideo(upcoming, upcoming[0]);
                service.updateServerPlaylist();
            }
            $rootScope.$apply();
        }


        this.bindPlayer = function (elementId) {
            console.info('Binding to ' + elementId);
            youtube.playerId = elementId;
        };

        this.createPlayer = function () {
            console.info('Creating a new Youtube player for DOM id ' + youtube.playerId + ' and video ' + youtube.videoId);
            return new YT.Player(youtube.playerId, {
                height: youtube.playerHeight,
                width: youtube.playerWidth,
                playerVars: {
                    rel: 0,
                    showinfo: 0
                },
                events: {
                    'onReady': onYoutubeReady,
                    'onStateChange': onYoutubeStateChange
                }
            });
        };

        this.loadPlayer = function () {
            console.info("LoadPlayer")
            console.info(youtube)
            if (youtube.ready && youtube.playerId) {
                if (youtube.player) {
                    youtube.player.destroy();
                }
                youtube.player = service.createPlayer();
            }
        };

        this.launchPlayer = function (video) {
            youtube.player.loadVideoById(video.id);
            youtube.videoId = video.id;
            youtube.videoTitle = video.title;
            youtube.uploader = video.uploader;
            youtube.duration = video.duration;

            return youtube;
        }

        this.listResults = function (data) {
            results.length = 0;
            for (var i = data.items.length - 1; i >= 0; i--) {
                results.push({
                    id: data.items[i].id.videoId,
                    title: data.items[i].snippet.title,
                    description: data.items[i].snippet.description,
                    thumbnail: data.items[i].snippet.thumbnails.default.url,
                    author: data.items[i].snippet.channelTitle
                });
            }
            return results;
        }

        this.queueVideo = function (video) {

            upcoming.push(video);

            if(youtube.state=='ended' && upcoming.length<=1)
            {

                service.launchPlayer(upcoming[0]);
                service.archiveVideo(upcoming[0]);
                service.deleteVideo(upcoming, upcoming[0]);
            }

            return upcoming;
        };

        this.archiveVideo = function (video) {
            history.unshift(video);
            return history;
        };

        this.getApiKey = function(){
            return ApiKey;
        }
        this.deleteVideo = function (list, video) {

            for (var i = list.length - 1; i >= 0; i--) {
                if (list[i].id === video.id) {
                    list.splice(i, 1);
                    break;
                }
            }
        };

        this.getYoutube = function () {
            return youtube;
        };



        this.getUpcoming = function () {
            return upcoming;
        };

        this.getHistory = function () {
            return history;
        };

        this.getVideoData= function(videoId){
            var deferred = $q.defer();
            $http.get(links.getVideoData(videoId))
                .then(function (response) {
                    deferred.resolve(response.data);
                });

            return deferred.promise;
        }


        this.getWebSocket = function(){
            return websocket;
        }

        this.setWebsocket = function(ws)
        {
            websocket = ws;


            websocket.onopen = function()
            {
                console.info("Web Socket is connected!!");
            };
            websocket.onmessage = function (evt)
            {
                var websocketMessage =  JSON.parse(evt.data);
                console.info(websocketMessage);
                if(websocketMessage.messageType == 'YoutubeRequest')
                {
                    var data = websocketMessage.message;
                    if (data.items.length > 0) {
                        var video = {
                            'id': data.items[0].id,
                            'title': data.items[0].snippet.title,
                            'duration': data.items[0].contentDetails.duration,
                            'uploader': data.uploader
                        };

                        service.queueVideo(video);
                    }

                }else{
                    //also check if playing
                    console.info("getting Current Song");
                    if(service.getYoutube().videoId) {
                        service.getVideoData(service.getYoutube().videoId).then(function(result){

                            var currentSong = {"messageType": 'CurrentSong', "playlist": [result]};
                            console.info(currentSong);
                            websocket.send(JSON.stringify(currentSong));
                        })

                    }
                }

            };
            this.websocket.onclose = function()
            {
                console.info("Connection is closed...");
            };
        }


    }
    angular
        .module('com.palepail.website.navigation')
        .service('YoutubeService', ['$window','$rootScope','$log','$http','$q', YoutubeService]);
})();