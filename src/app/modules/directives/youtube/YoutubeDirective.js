/**
 * Created by palepail on 8/16/2015.
 */


(function() {
    function youtube($window, $interval, YoutubeService) {
        return {
            restrict: 'E',
            templateUrl: 'YoutubeView.html',
            scope: {
                state: '=',
                currentTime: '=',
                externalAdd:'='
            },
            link: function($scope, $http, $log){

                init();


                function init() {

                    var tag = document.createElement('script');
                    tag.src = "http://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                    YoutubeService.loadPlayer();
                    $scope.youtube = YoutubeService.getYoutube();
                    $scope.youtube.state = 'ended';

                    $scope.upcoming = YoutubeService.getUpcoming();
                    $scope.history = YoutubeService.getHistory();
                    $scope.playlist = true;
                }

                $scope.getCurrentTime = function(){
                    var time = moment.duration($scope.youtube.player.getCurrentTime(), 'seconds').format("h:mm:ss");
                    $scope.apply();
                    if(time==0)
                    {
                        return '';
                    }
                    return time;
                }

                $scope.externalAdd = function(video){
                    console.info("externalAdd")
                    if(video) {
                        $scope.queue(video);
                    }
                };



                $scope.formatDuration = function(duration){
                    var time = moment.duration(duration, 'seconds').format("h:mm:ss");
                  if(time==0)
                  {
                      return '';
                  }
                    return time;
                }


                $scope.launch = function (video) {
                    YoutubeService.launchPlayer(video);
                    YoutubeService.archiveVideo(video);
                    YoutubeService.deleteVideo($scope.upcoming, video);
                    console.info('Launched id:' + video.id + ' and title:' + video.title);
                };

                $scope.queue = function (video) {

                    YoutubeService.queueVideo(video);
                    YoutubeService.deleteVideo($scope.history, video);


                    console.info('Queued id:' + video.id + ' and title:' + video.title);
                };

                $scope.delete = function (listName, video) {

                    var list;
                    if(listName=='upcoming')
                    {

                        list = YoutubeService.getUpcoming();
                    }
                    else{

                        list = YoutubeService.getHistory();
                    }
                    YoutubeService.deleteVideo(list, video);
                };

                $scope.search = function () {
                    $http.get('https://www.googleapis.com/youtube/v3/search', {
                        params: {
                            key: YoutubeService.getApiKey(),
                            type: 'video',
                            maxResults: '8',
                            part: 'id,snippet',
                            fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails/default,items/snippet/channelTitle',
                            q: this.query
                        }
                    })
                        .success( function (data) {
                            YoutubeService.listResults(data);
                            $log.info(data);
                        })
                        .error( function () {
                            $log.info('Search error');
                        });
                }

                $scope.tabulate = function (state) {
                    $scope.playlist = state;
                }
            },

        };
    }
    angular
        .module('com.palepail.website.directives')
        .directive('youtube', ['$window','$interval','YoutubeService',youtube]);
})();