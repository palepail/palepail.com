/**
 * Created by palepail on 7/28/2015.
 */

(function() {
    function srcRefresh() {
        return {
            restrict: 'A',
            scope: {
                refresh: "=refresh"
            },
            link: function (scope, element, attr) {
                var refreshMe = function () {
                    element.attr('src', element.attr('src'));
                };

                scope.$watch('refresh', function (newVal, oldVal) {
                    if (scope.refresh) {
                        scope.refresh = false;
                        refreshMe();
                    }
                });
            }
        };
    }
    angular
        .module('com.palepail.website.directives')
        .directive('srcRefresh', [srcRefresh]);
})();
