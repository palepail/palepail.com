/**
 * Created by derek.kam on 6/29/2015.
 */
(function() {
    function rotate() {
        return {
            link: function(scope, $interval, element, attrs) {
                var degree = 0;

                $interval(rotate(attrs.degrees), 1000);
                function rotate(rotateDegrees){
                    degree+=rotateDegrees;
                    if(degree>360)
                    {
                        degree = degree%360;
                    }
                    element.css({
                        '-moz-transform': 'rotate(' + degree + 'deg)',
                        '-webkit-transform': 'rotate(' + degree + 'deg)',
                        '-o-transform': 'rotate(' + degree + 'deg)',
                        '-ms-transform': 'rotate(' + degree + 'deg)'
                    });
                }

            }
        };
    }
    angular
        .module('com.palepail.website.directives')
        .directive('rotate', [rotate]);
})();
